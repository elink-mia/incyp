$(document).ready(function() {
    var filterSingle = $('.filtr-container').filterizr({
        layout: 'sameWidth',
        setupControls: false
    });
    $('.grid-filter li').click(function () {
        $('.grid-filter li').removeClass('active');
        $(this).addClass('active');
        var filter = $(this).data('filter');
        filterSingle.filterizr('filter', filter);
    });
    $('.filter-select').on('change', function() {
        // get filter value from option value
        var filterValue = this.value;
        filterValue = $('.filtr-item')[filterValue] || filterValue;
        //use filterFn if matches value
        filterSingle.filterizr('filter', filterValue);
    });
});