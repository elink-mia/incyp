;
(function() {

    'use strict';
    $(function() {

        // iPad and iPod detection  
        var isiPad = function() {
            return (navigator.platform.indexOf("iPad") != -1);
        };

        var isiPhone = function() {
            return (
                (navigator.platform.indexOf("iPhone") != -1) ||
                (navigator.platform.indexOf("iPod") != -1)
            );
        };

        var gutters = function() {
            $(document).ready(function() {
                var headerH = $('#header').height(),
                    winW = $(window).width(),
                    h8 = headerH - 8;
                $('#gutters').css({
                    "border-width": 8 + "px"
                });
                $('#main').css('margin-top', h8);
                if (winW < 992) {
                    $('.offcanvas-collapse').css('top', headerH);
                }
            });
        };

        // Burger Menu
        var burgerMenu = function() {
            $('.mob-menu').click(function() {
                $('.offcanvas-collapse').toggleClass('open');
                if ($('.offcanvas-collapse').hasClass('open')) {
                    $('.mob-menu').addClass('active');
                } else {
                    $('.mob-menu').removeClass('active');
                }
                $('body').toggleClass("no-scroll");
            });
        };

        var windowScroll = function() {
            $(window).scroll(function() {
                var header = $('#header'),
                    scroll = $(window).scrollTop();
                if (scroll >= 80) {
                    $(header).addClass('fixed');
                } else {
                    $(header).removeClass('fixed');

                }
            });
        };

        var banner = function() {
            $('.banner').slick({
                autoplay: true,
                autoplaySpeed: 2000,
                dots: true,
                arrows: false,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear'
            });
        };

        var marquee = function() {
            function Mar(id, id1, id2) {
                var speed = 46;
                var chg_s = 4500;
                var timer;
                var chg_timer;
                var ad_carousel_mar1 = document.getElementById(id1);
                var ad_carousel_mar2 = document.getElementById(id2);
                var ad_carousel = document.getElementById(id);
                ad_carousel_mar2.innerHTML = ad_carousel_mar1.innerHTML;
                function picMarquee() {
                    ad_carousel.scrollTop = ((ad_carousel_mar2.offsetHeight - ad_carousel.scrollTop) <= 0) ? 0 : parseInt(ad_carousel.scrollTop, 10) + 1;
                }
                timer = setInterval(picMarquee, speed);
                ad_carousel.onmouseover = function() {
                    clearInterval(timer);
                }
                ad_carousel.onmouseout = function() {
                    timer = setInterval(picMarquee, speed);
                }
            }
            Mar('mar-vd', 'mar-vd1', 'mar-vd2');
        };

        var goToTop = function() {

            $('.js-gotop').on('click', function(event) {

                event.preventDefault();

                $('html, body').animate({
                    scrollTop: $('html').offset().top
                }, 500, 'easeInOutExpo');

                return false;
            });

            $(window).scroll(function() {

                var $win = $(window);
                if ($win.scrollTop() > 200) {
                    $('#bk-top').addClass('active');
                } else {
                    $('#bk-top').removeClass('active');
                }

            });

        };
        // gutters();
        burgerMenu();
        // windowScroll();
        banner();
        marquee();
        goToTop();


    });


}());