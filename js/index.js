$(document).ready(function() {
    $("#vendor .item").slice(0, 3).show();
    $("#vendor .loadMore").on('click', function(e) {
        e.preventDefault();
        $("#vendor .item:hidden").slice(0, 3).slideDown();
        if ($("#vendor .item:hidden").length == 0) {
            $("#vendor .loadMore").fadeOut('fast');
        }
    });
    $("#category .item").slice(0, 4).show();
    $("#category .loadMore").on('click', function(e) {
        e.preventDefault();
        $("#category .item:hidden").slice(0, 4).slideDown();
        if ($("#category .item:hidden").length == 0) {
            $("#category .loadMore").fadeOut('fast');
        }
    });
});