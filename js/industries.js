$(document).ready(function() {
    $('.ydd-img').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        infinite: true,
        asNavFor: '.ydd-nav',
        adaptiveHeight: true
    });
    $('.ydd-nav').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.ydd-img',
        arrows: true,
        dots: true,
        infinite: true,
        vertical: true,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                autoplay: false
            }
        }]
    });
    $("#category .item").slice(0, 4).show();
    $("#category .loadMore").on('click', function(e) {
        e.preventDefault();
        $("#category .item:hidden").slice(0, 4).slideDown();
        if ($("#category .item:hidden").length == 0) {
            $("#category .loadMore").fadeOut('fast');
        }
    });
});